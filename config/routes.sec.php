<?php

/* Router */
class Routes {
    
    /* Get info that was send by Telegram */
    public function getOutput() {
        return json_decode(file_get_contents('php://input'), TRUE);
    }

    /* Working with router */
    public function rowRoute() {
        $output = self::getOutput();
        if($output['message'] != null) {
            if($output['message']['text'] == '/start') {
                return 'main/start/null';
            }
            else {
                return 'none';
            }
        }
        elseif($output['callback_query'] != null) {
            return $output['callback_query']['data'];
        } else {
            return 'err';
        }
    }

    /* Connecting to Controller */
    public function getController() {
        
        $rr = $this::rowRoute();
        
        if($rr == 'none') {
            $tdata["sticker"] = "CAADBAADcwEAAuJy2QABTawAAcpGWf5OAg";
            
            $data = json_encode($tdata, JSON_FORCE_OBJECT);

            $comm = new Comm($data);
            $comm->mSendSticker();

            return true;
        } elseif($rr == 'err') {
            return true;
        }

        $c = explode("/", $this::rowRoute());

        require_once "controllers/".$c[0].".sec.php";
            
        $cl = ucfirst($c[0]);
        $fun = $c[1];
        $par = ($c[2] == "null" ? null : $c[2]);

        $cObj = new $cl ;
        call_user_func_array(array($cObj, $fun), array($par));

    }

}