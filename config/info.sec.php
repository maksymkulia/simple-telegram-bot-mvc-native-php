<?php

/* Configuration Class */
class Config
{
    const TOKEN = "YOUR_TOKEN_FROM_BOTFATHER";
    const API = 'https://api.telegram.org/bot' . self::TOKEN;
}