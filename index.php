<?php

/* Show err */
ini_set('display_errors',1);
error_reporting(E_ALL);

/* Connection of assets */
require_once "config/info.sec.php";
require_once "assets/db.sec.php";
require_once "config/routes.sec.php";

/* Models */
require_once "models/communication.sec.php";

/* Connecting Router */
$route = new Routes();
$route->getController();