# Simple Telegram Bot MVC Native PHP

## About
Simple framework on the native PHP based on MVC pattern for Telegram Chat Bot Construction based on [Telegram API](https://core.telegram.org/bots).

## Status
Not optimized, active development. Still raw, but working.

## Working Projects
- [Dance Studio Bot](https://t.me/BondarenkoStudioBot);
- [Add your Bot](https://t.me/soicame).

## TODO
- Optimize `/models/communication.sec.php` to cover in one method both `$output['message']` and `$output['callback_query']`;
- In `/models/communication.sec.php` add more Fields for [Message Telegram Type](https://core.telegram.org/bots/api#message);
- In `/assets/db.sec.php` add more convenient methods for the database (UPDATE, DELETE);
- In `/config/routes.sec.php` optimeze Router Class. Make it more tiny and clean.

## What will you need?
- Telegram account;
- Hosting/Server that supports PHP, SSL setup and has MySQL Database;
- Domain;
- SSL Certificate (Let's Encrypt will be OK).

## How to create your first Bot?
1. Create account on Telegram;
2. Search for [BotFather](https://t.me/BotFather) and Start it;
3. Type command `/newbot`;
4. Follow instructions;
5. Save your API Token of your new Bot;
6. Now you need to setup your Bot to work with your Server via webhooks. To do it I recommend you to create sub-domain, example: `tg.example.com`. It is important to make SSL for your sub-domain;
7. Open browser and go via this link: api.telegram.org/bot`my_bot_token`}/setWebhook?url=`tg.example.com`/index.php. [Good example](https://medium.com/@xabaras/setting-your-telegram-bot-webhook-the-easy-way-c7577b2d6f72);
8. Ok, now you can just copy-paste this project to your server so that it should be available by link `https://tg.example.com/index.php`;
9. Open file `/assets/db.sec.php` and fill your Database parameters;
10. Open file `/config/info.sec.php` and fill your API Token;
11. Now open your Chat Bot and enter `/start`. You should see `It works!`!

## What it can do?
- Setup routes based on MVC pattern `controller/action/variable`;
- Ready methods for simple messages and messages with keyboard;
- Connection to DB and convenient DB methods;

## Routes rule
Example: If you want to create route to products list and make it available from callback button you should do the following:
- Create button in the keyboard (you can find example in `/controllers/main.sec.php`->`index()`). With the following route: `products/list/null`;
- In folder `controllers` create file `products.sec.php` with class `Products` and action `list()`.

## Questions/Ideas?
You can write me to my [Telegram](https://t.me/soicame).