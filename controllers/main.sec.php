<?php

/* Construction for main Controller */
class Main
{
    /* /start */
    public function start() {
        // Create button
        $inline_button1_1 = array("text"=>"Main Page","callback_data"=>'main/index/null');
        // Add button to keyboard
        $inline_keyboard = [[$inline_button1_1]];
        // Create message
        $tdata["message"] = "It Works!";
        // Add keybord to message markdown
        $tdata["markup"] = json_encode(array("inline_keyboard"=>$inline_keyboard));
        // Prepare data for send message
        $data = json_encode($tdata, JSON_FORCE_OBJECT);

        $comm = new Comm($data);
        $comm->mSendMarkupMessage();

        return true;
    }

    /* Index */
    public function index() {
        // Create button
        $inline_button1_1 = array("text"=>"Main Page","callback_data"=>'main/index/null');
        // Add button to keyboard
        $inline_keyboard = [[$inline_button1_1]];
        // Create message
        $tdata["message"] = "Hello world!";
        // Add keybord to message markdown
        $tdata["markup"] = json_encode(array("inline_keyboard"=>$inline_keyboard));
        // Prepare data for send message
        $data = json_encode($tdata, JSON_FORCE_OBJECT);

        $comm = new Comm($data);
        $comm->cSendMarkupMessage();

        return true;
    }

}