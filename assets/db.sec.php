<?php

/* Database Class */
class Db
{
    /**
     * Setting up DB connection
     * @return array <p>DB connection</p>
     */
    public static function getConnection() {
        // DB Params
        $db_name = "HERE_GOES_DB_NAME";
        $db_user = "HERE_GOES_DB_USER_NAME";
        $db_user_pass = "HERE_GOES_DB_USER_PASS";

        // Setting connection to DB
        $dsn = "mysql:host=localhost;dbname=$db_name";
        $db = new PDO($dsn, "$db_user", "$db_user_pass");

        // Set UTF-8 encoding
        $db->exec("set names utf8");

        return $db;
    }
    /**
     * Select that expect 1 row
     * @return array <p>Request result</p>
     */
    public static function sOne($params, $table, $where) {
        // Connection to DB
        $db = self::getConnection();
        // Request
        $sql = "SELECT $params FROM $table WHERE $where";

        // Prepare request
        $result = $db->prepare($sql);
        $result->execute();

        // Return request
        $selectData = $result->fetch();

        return $selectData;
    }
    /**
     * Select that expect many rows
     * @return array <p>Request result</p>
     */
    public static function sMany($params, $table, $where) {
        // Connection to DB
        $db = self::getConnection();

        $selectData = array();
        // Request
        $sql = "SELECT $params FROM $table WHERE $where";

        // Prepare request
        $result = $db->query($sql);
        // Return request with indexes
        $selectData = $result->fetchAll(PDO::FETCH_ASSOC);

        return $selectData;
    }
    /**
     * Select that expect raw request
     * @return array <p>Request result</p>
     */
    public static function sRaw($req) {
        // Connection to DB
        $db = self::getConnection();

        $selectData = array();
        // Request
        $sql = $req;

        // Prepare request
        $result = $db->query($sql);
        // Return request with indexes
        $selectData = $result->fetchAll(PDO::FETCH_ASSOC);

        return $selectData;
    }
}