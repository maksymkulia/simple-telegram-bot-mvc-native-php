<?php

/* Communication class */
class Comm
{   
    public $data;

    function __construct($data = null) {
        $this->data = $data;
    }
    /* getMe method */
    public function getMe() {
        $res = file_get_contents(Config::API."/getMe");
        return $res;
    }
    /* Sending simple message M */
    public function mSendSimpleMessage() {
        $output = Routes::getOutput();
        $m = json_decode($this->data, TRUE);
        
        $chat_id = $output['message']['chat']['id'];
        $message = urlencode($m["message"]);

        file_get_contents(Config::API."/sendMessage?chat_id=$chat_id&text=$message&parse_mode=html");
        return true;
    }
    /* Sending markup message M */
    public function mSendMarkupMessage() {
        $output = Routes::getOutput();
        $m = json_decode($this->data, TRUE);
        
        $chat_id = $output['message']['chat']['id'];
        $message = urlencode($m["message"]);
        $markup = $m["markup"];

        file_get_contents(Config::API."/sendMessage?chat_id=$chat_id&text=$message&reply_markup=$markup&parse_mode=html");
        return true;
    }
    /* Sending sticker M */
    public function mSendSticker() {
        $output = Routes::getOutput();
        $m = json_decode($this->data, TRUE);
        
        $chat_id = $output['message']['chat']['id'];
        $sticker = $m["sticker"];

        file_get_contents(Config::API."/sendSticker?chat_id=$chat_id&sticker=$sticker&parse_mode=html");
        return true;
    }

    /* Sending simple message C */
    public function cSendSimpleMessage() {
        $output = Routes::getOutput();
        $m = json_decode($this->data, TRUE);
        
        $chat_id = $output['callback_query']['message']['chat']['id'];
        $message = urlencode($m["message"]);

        file_get_contents(Config::API."/sendMessage?chat_id=$chat_id&text=$message&parse_mode=html");
        return true;
    }
    /* Sending markup message C */
    public function cSendMarkupMessage() {
        $output = Routes::getOutput();
        $m = json_decode($this->data, TRUE);
        
        $chat_id = $output['callback_query']['message']['chat']['id'];
        $message = urlencode($m["message"]);
        $markup = $m["markup"];

        file_get_contents(Config::API."/sendMessage?chat_id=$chat_id&text=$message&reply_markup=$markup&parse_mode=html");
        return true;
    }

}